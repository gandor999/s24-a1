// Find users with s in firstname and d in lastname

db.users.find(
	{ 
		$or: [
				{ "firstName": { $regex: "s", $options: '$i' } }, 
				{ "lastName": { $regex: "d", $options: '$i' } }
			] 
	},
		{ "firstName": 1, "lastName": 1, "_id": 0 }
);


// Find users that are in HR department and are greater than or equal to 70
db.users.find(
	{ 
		$and: [
				{ "department": "HR" }, 
				{ "age": { $gte: 70 } }
			]
	}
);


// Find users that are have e in their firstname and age is less than or equal to 30
db.users.find(
	{ 
		$and: [	
				{ "firstName": { $regex: "e", $options: '$i' } }, 
				{ "age": { $lte: 30 } }
			] 
	}
);




